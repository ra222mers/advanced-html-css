document.querySelector(".dropdown").addEventListener("click", function(event) {
    if (event.target === this) {
        document.querySelector(".dropdown__list").classList.toggle("dropdown__list--active");
        this.classList.toggle("dropdown--active");
    }
});