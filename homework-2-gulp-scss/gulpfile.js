const gulp = require("gulp");
const clean = require('gulp-clean');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const concatCss = require('gulp-concat-css');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();
 
sass.compiler = require('node-sass');

// BUILD FUNCTIONS START

gulp.task("cleanDist", function() {
    return gulp.src('./dist', {read: false})
            .pipe(clean());
});

gulp.task("compileSCSS", function() {
    return gulp.src("./src/styles/*.scss")
            .pipe(sass().on('error', sass.logError))
            .pipe(concatCss("styles.min.css"))
            .pipe(autoprefixer({
                cascade: false
            }))
            .pipe(cleanCSS({
                compatibility: 'ie8',
                level: 2
            }))
            .pipe(gulp.dest("./dist/"))
});

gulp.task("gatherJS", function() {
    return gulp.src("./src/scripts/**/*.js")
            .pipe(concat('scripts.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest("dist/"))
});

gulp.task("optimiseImg", function() {
    return gulp.src('./src/images/**')
            .pipe(imagemin())
            .pipe(gulp.dest('dist/images/'))
});

gulp.task("build", gulp.series("cleanDist", "compileSCSS", "gatherJS", "optimiseImg"));

// BUILD FUNCTIONS END

// DEV FUNCTIONS START

gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    browserSync.watch("./dist", browserSync.reload);
});

gulp.task("watchSCSS-JS", function() {
    gulp.watch("./src/styles/**/*.scss", gulp.series("compileSCSS"));
    gulp.watch("./src/scripts/**/*.js", gulp.series("gatherJS"));
})

gulp.task("dev", gulp.parallel("serve", "watchSCSS-JS"));

// DEV FUNCTIONS END